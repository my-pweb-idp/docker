CREATE DATABASE IF NOT EXISTS `pweb`;
USE `pweb`;

CREATE TABLE `location` (
  `id` int NOT NULL AUTO_INCREMENT,
  `latitude` varchar(45) NOT NULL,
  `longitude` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `product_type` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `product_type` VALUES (1,'Fantasy',NULL),(2,'Science Fiction',NULL),(3,'Action & Adventure',NULL),(4,'Mystery',NULL)
,(5,'Biography',NULL),(6,'Children’s',NULL),(7,'LGBTQ+',NULL),(8,'Romance',NULL);

CREATE TABLE `profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone_number` varchar(45) DEFAULT ' ',
  `photo_URL` varchar(45) DEFAULT NULL,
  `location_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);

CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `owner_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `author` varchar(145) NOT NULL,
  `images_path` varchar(200) NOT NULL,
  `type_id` int NOT NULL,
  `location_id` int DEFAULT NULL,
  `SYS_CREATION_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `SYS_UPDATE_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_product_type_id_idx` (`type_id`),
  CONSTRAINT `fk_product_type_id` FOREIGN KEY (`type_id`) REFERENCES `product_type` (`id`)
);

CREATE TABLE `status` (
  `id` int NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `status` VALUES (1,'Pending',NULL),(2,'Accepted',NULL),(3,'Rejected',NULL);

CREATE TABLE `requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `request_profile_id` int NOT NULL,
  `status_id` int DEFAULT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `SYS_CREATION_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `SYS_UPDATE_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`product_id`,`request_profile_id`,`status_id`),
  KEY `status_id_FK_idx` (`status_id`),
  CONSTRAINT `status_id_FK` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
);

CREATE TABLE `favorite` (
  `id` int NOT NULL AUTO_INCREMENT,
  `profile_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_profile_idx` (`profile_id`),
  KEY `FK_product_idx` (`product_id`),
  CONSTRAINT `FK_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_profile` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`)
);