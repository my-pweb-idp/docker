# Docker

https://www.figma.com/file/oyddJG2uozfqGDhMZVmbpt/Pweb?node-id=114%3A1359

## How to run

docker swarm init

docker stack deploy -c portainer-stack.yml pwebIDP

docker stack ps pwebIDP   localhost:9000

docker compose up -d

## Config gitlab runner

docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register

https://gitlab.com/

docker run -it -v /srv/gitlab-runner/config:/test alpine

docker restart gitlab-runner 

"/var/run/docker.sock:/var/run/docker.sock"
